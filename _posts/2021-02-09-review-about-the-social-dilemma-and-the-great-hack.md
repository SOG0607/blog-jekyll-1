---
layout: post
date: 2021-02-09 10:20
title: Review about "The Social Dilemma" and "The Great Hack”
category: Review
author: Kiruthiga K
banner-url: "https://www.deccanherald.com/sites/dh/files/article_images/2019/07/26/the-great-hack-1564145953.jpg"
---


#### Review about Documentaries "The Social Dilemma" and "The Great Hack”

Social Media like Facebook, Twitter, Snapchat, Instagram and etc.,. have become the platforms for conducting psychological experiments known as *Psychops* (psychological operations) to influence the behaviour of individuals. This is known as **Behavioural psychology**.

>   It is an approach in psychology, which studies an individual's observable behaviour

Such experiments are considered illegal because the data collected by these platforms is sold to **Advertisers/companies** which implies that we are the *products* (since data collected about us is 'us') and the platforms are the *consumers*. These processes question the *authenticity, integrity and confidentiality* (CIA triads) of the user's data. The data collected by the platforms is used to build and deploy a model which collects and interprets data through **Machine learning** commonly known as the *trial and error method* by making predictions from the previously stored data(even though there may be some predictions which have are wrong......still, data collected, is collected), each individual has a separate model.

>   Although data is being collected for a huge mass of people, only a considerable amount of people can be easily manipulated psychologically. 

The data collected is then analysed, interpreted which is used to persuade a certain group of people who can be easily persuaded to achieve certain goals like in **electoral politics**(election campaigns) and to **maximize the profit** gained from ads which in turn is used to manipulate the individuals, who are called the *'persuadable'*. They collect data when we screen check often, we are made to do this by persuasion. This practice is known as **'positive intermittent reinforcement'**, it is a part of behavioural psychology. It tells, that this kind of 'screen checking' behaviour, is similar to that of the casino and slot machines(prominently found in Las Vegas), where the user keeps repeating the process of playing, again and again due to the belief that something positive will happen(in this case he/she wins), similarly we keep checking our devices for 'new things' often(having a notion that they will be positive), even though we do not have any work in the device, which in turn increases our screen time(influencing our on-screen behaviour and affecting our off-screen behaviour). This kind of activity is more resistant to extinction.

>   Behaviours can be influenced by rewards or punishments. We work sincerely if we acknowledge that we get a reward when we complete a particular task along with the realization that we get the reward only when we complete the task. So, that seems a bit discouraging to us right?. So how do we keep ourselves engaged in the task even though we are uncertain about whether we will get a reward or not? 

>   When we know to expect the reward after taking a certain action, we tend to work less for it. Yet when the timing of the reward or the certainty that we’ll get it at all is unpredictable, we tend to repeat that behavior with even more enthusiasm, in hope for the end result. We relish the joy of a “hard-earned” reward that much more.

>    Source - Thought Catalog

Want to know more? Click [here](https://psychopathsandlove.com/intermittent-reinforcement/)

Positive intermittent reinforcement or our screen presence, is increased and achieved by, displaying *advertisements* personalized(by the data collected) for us, *click baits*(they contain algorithms which triggers emotions in the user), *notifications.* Even though data collection is considered illegal regarding privacy, it has some benefits, like data collection helps to personalize things relevant for us, it is still illegal, because the thing in psychology is that we are tempted to exploit, manipulate things when we get hold of information about that particular thing. The icing on the cake is that Humans (the ones who created the models) have absolutely no control over what the machine (artificial intelligence) learns through machine learning, which paves way for AI's to rule over humans. But thing is, there is no such thing called as **'complete security'** when it comes to the internet and cyber stuff. Every security system has got its own loopholes. It is irony, that these networks which were created to connect people across the world are the ones dividing people mentally which affects them physically and manipulating them gradually without their acknowledgement(Divide and conquer theory). Another thing is that these psychological manipulations have negatively affected our physiological state. Statistical reports have shown the emergence of *'Snapchat Dysmorphia'*(since the introduction of **'Snapchat filters'**, in which the individuals go for plastic surgeries with the main aim of coming on par with the snapchat filters), increase in Suicidal rates in pre-teen, addiction to devices etc.,. We have to reconsider and think how social network should have to work and develop an alternative model.
